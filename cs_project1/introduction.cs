﻿using System;

namespace IntroCS
{
   public class Introduction
   {

      // Constructor
      public Introduction ()
      {
      }

      public string IntroMenu ()
      {
         string response = "";
         // Display introduction menu, let user browse instructions and return
         // to the main menu as much as they wish. 
         while (response != "N") {
            response = Header ();
            if (response == "I") {
               Console.Clear ();
               Instructions ();
               Console.Clear ();
            } else if (response == "E") {
               Environment.Exit (0);
            } else {
               Console.Clear ();
            }
         }

         string name = UI.PromptLine ("Enter character name: ").Trim ();
         Console.WriteLine ();
         return name;
      }
         
      public string Header ()
      {
         Console.ForegroundColor = ConsoleColor.Green;
         Console.Write (@"===============================================================================
============= Welcome to Dungeon Crawler - A text based adventure =============
======================= game created by Team Rocket ===========================");
         Console.WriteLine ();

         Console.ForegroundColor = ConsoleColor.Cyan;
         Console.Write (@"
The Map of the Dungeon will guide you to the Amulet that has been hidden for 
                centuries by the Almighty Robert Yacobellis.");
         Console.ForegroundColor = ConsoleColor.Red;
         Console.WriteLine (@"
                  _____________________________
                 |                             |
                 |___     _______________      |
                     |   |          ___  |     |______
                     |___|         |   | |            |
                                   |   | |_______     |________
              ____D__U  N  G  E__O_|N  C  R  A  W| L  E  R     |
                      |      |         |         |________     |
              _____   |______|         |         |             |
                   |               |   |  _______|       ______|
                   |____      _____|   | |              |
                        |    |     |   |_|________      |
                        |____|     |                    |        
                                   |____________________|
");
         Console.Write (@"
==================== Please select an option to continue! =====================
================= N=New Game ===== I=Instructions ==== E=Exit =================
");
         string response = UI.PromptLine ("").Trim().ToUpper();

         if (response == "I") {
            return "I";
         } else if (response == "E") {
            return "E";
         } else if (response == "N") {
            return "N";
         } else {
            return "";
         }
      }

      public void Instructions ()
      {
         Console.WriteLine (@"Within Dungeon Crawler, you'll be tasked with retrieving the Amulet of Yendor.

~ MOVEMENT ~
In order to move around in the game, use commands: 

   'go DIRECTION' 

where DIRECTION is 'east', 'south', 'west', or 'north'.

~ COMBAT ~
Combat within Dungeon Crawler occurs in two ways -- against minions or bosses.
When fighting minions, the player is presented with a sequence, such as
         f
         ?
         2
         $ 
The player must replicate the sequence after it is displayed to successfully
deal damage to the enemy. Alternatively, the player will be confronted with
bosses, in which case he or she must answer a riddle with a single-word 
answer in order to defeat the boss and continue with the game. 
While health is reset to 100 after every battle, getting hit too many times 
in a single battle will cause death and end the game.

Press Enter to return to the main menu. ");
         Console.ReadLine ();
      }
   }
}