﻿using System;

namespace IntroCS
{
   class Health
   {
      private int health; 

      // Constructor
      public Health(int health)
      {        
         this.health = health;
      }

      //subtractHealth chunk
      // This method subtracts a certain amount of health
      // from the player and displays however much is left. 
      public void subtractHealth(int num)
      {
         health = health - num;
         Console.WriteLine (@"
You take {0} damage!
Current health: {1} / 100
", num, health); // Out of convention, health is defaulted to 100. 
      }

      // Adds health by specified amount.
      public void addHealth(int num)
      {
         health = health + num;
         Console.WriteLine (@"Your health is replenished by {0}!
Current health: {1} / 100", num, health);
      }

      // Returns current health value.
      public int GetHealth()
      {
         return health;

      }

      // Checks to see whether health is still above 0. If it reaches
      // 0 or drops below 0 and a checkHealth is performed, the game
      // will end. 
      public void checkHealth(Health health)
      {
         if (health.health <= 0) {
            Console.WriteLine ("\nYou are dead!\n");
            Environment.Exit (0);
         }
      }
   }
}