﻿/// This program demonstrates the use of the stopwatch and 
/// provides the code necessary to implement a combat system
/// that requires memorization in order to defeat a set of goblins.
using System;
using System.Diagnostics;
using System.Threading;
using System.Collections.Generic;

namespace IntroCS
{
   class Enemy // Implement interface for dealing with minions/bosses
   {
      // Now for the constructor
      public Enemy()
      {
      }

      //combatLettersequence chunk
      /// This function has the player fight a number of goblins, dependant
      /// on what the argument value was. They get more and more difficult. If 
      /// the player is unable to faithfully replicate the sequence presented 
      /// to him or her, they have health subtracted and are allowed to try again.
      public void combatLetter(int enemies)
      {
         Console.WriteLine (@"You stumble into a group of goblins -- horrid creatures, faces scarred
beyond recognition and carrying rusted, dull swords.");
         var health = new Health (100);
         var letterSequence = new List<char> ();
         var stopwatch = new Stopwatch ();
         string letterstring = "";
         string userline = "empty!";
         // Now we engage the battle sequence.
         for (int i = 0; i < enemies; i++) {
            Console.Write (@"You get ready to fight the nearest goblin. 
Press Enter to Continue. ");
            Console.ReadLine ();
            letterstring = "";
            while (userline.Trim ().ToUpper () != letterstring) {
               letterstring = "";
               for (int j = 0; j < i+1; j++) { // The amount of letters presented depend on the goblin number
                  letterSequence.Add (generateUpperletter ()); // Generates uppercase letter, returns as char
                  var chartobedisplayed = letterSequence [letterSequence.Count - 1];
                  Console.Write ("\n    {0}", chartobedisplayed);
                  stopwatch.Start ();
                  // Now that the letter has been displayed for a certain amount of time, clear the line
                  // and display the next letter.
                  while (stopwatch.Elapsed.TotalSeconds < 1.0) {
                     FlushKeyBoardInput ();
                  }
                  // Clear line and reset stopwatch
                  Console.SetCursorPosition (0, Console.CursorTop);
                  ClearCurrentConsoleLine ();
                  stopwatch.Reset ();
                  // Repeat this j times
               }
               // Now we prompt the user for the correct sequence of numbers.
               if (letterSequence.Count == 1) {
                  letterstring = Convert.ToString (letterSequence [0]);
               } else {
                  for (int n = 0; n < letterSequence.Count; n++) {
                     letterstring += Convert.ToString (letterSequence [n]);
                  }
               }
               userline = UI.PromptLine ("Enter the correct sequence: ");
               if (userline.Trim ().ToUpper () == letterstring) {
                  Console.WriteLine ("You swing your sword, breaking through armor and shearing flesh from bone!\n");
               } else {
                  Console.WriteLine ("The goblin's sword cuts into you!");
                  // Implement reduction in health here
                  health.subtractHealth (20);
                  health.checkHealth (health);
                  Console.WriteLine ("Press Enter to attempt another attack. ");
                  Console.ReadLine ();
               }
               letterSequence.Clear ();
            }
         }
         Console.WriteLine ("Press Enter to continue.");
         Console.ReadLine ();
      }

      // Now to implement the six different bosses
      public void bossFirst(Health health)
      {
         // Chimera
         string response = "";
         Console.WriteLine ("The Chimera hisses before presenting you with its riddle: \n");

         while (response != "man" && response != "human" && response != "humans") {
            response = UI.PromptLine ("Which creature walks on four legs in the morning, two legs in the " +
            "afternoon,\nand three legs in the evening?\n\n> ").Trim ().ToLower ();
            if (response != "man" && response != "human" && response != "humans") {
               Console.WriteLine ("\n'Wrong,' it growls. The Chimera strikes, its snake-like fangs piercing" +
                  " into your skin before you manage to strike it with your sword. \n");
               health.subtractHealth (25);
               health.checkHealth (health);
               Console.WriteLine ("\nThe Chimera prompts you again:");
            }
         }
         Console.WriteLine ("\nUpon being defeated, the chimera hisses angrily and collapses. 'Go forth and " +
            "never return,' it growls.\n");
      }

      public void bossSecond(Health health)
      {
         // Raviel
         string response = "";
         Console.WriteLine ("Raviel siezes you with its talons and presents you with its riddle: \n");

         while (response != "window" && response != "windows" && response != "a window") {
            response = UI.PromptLine ("Which invention lets you see through a wall?\n> ").Trim ().ToLower ();
            if (response != "window" && response != "windows" && response != "a window") {
               Console.WriteLine ("'Wrong,' it growls. Raviel's talons grow tighter around your body.\n");
               health.subtractHealth (25);
               health.checkHealth (health);
               Console.WriteLine ("\nRaviel prompts you again:");
            }
         }
         Console.WriteLine ();
      }

      public void bossThird(Health health)
      {
         // Basilisk
         string response = "";
         Console.WriteLine ("The basilisk encircles you and prompts you with its riddle: \n");

         while (response != "coffin" && response != "coffins" && response != "a coffin") {
            response = UI.PromptLine ("Who makes it, yet has no need of it?\nWho buys it, yet has no use " +
               "for it?\nWho uses it, yet can neither see nor feel it?\nWhat is it?\n\n> ").Trim ().ToLower ();
            if (response != "coffin" && response != "coffins" && response != "a coffin") {
               Console.WriteLine ("\n'Wrong, wrong!' it hisses. The Basilisk strikes you.\n");
               health.subtractHealth (25);
               health.checkHealth (health);
               Console.WriteLine ("\nThe Basilisk prompts you again:");
            }
         }
         Console.WriteLine ("\n\n");
      }

      public void bossFourth(Health health)
      {
         Console.WriteLine ("ayy lmao");
      }

      // This function clears the current line in the Console
      public static void ClearCurrentConsoleLine ()
      {
         int currentLineCursor = Console.CursorTop;
         Console.SetCursorPosition (0, Console.CursorTop);
         Console.Write (new string (' ', Console.WindowWidth));
         Console.SetCursorPosition (0, currentLineCursor);
      }

      // This function prevents the user from inputting answers while
      // the sequence is displaying.
      public static void FlushKeyBoardInput ()
      {
         while (Console.KeyAvailable) {
            ConsoleKeyInfo key = Console.ReadKey (true);
            Console.Write ("");
         }
      }

      // This function generates a letter within the specified range.
      static char generateUpperletter ()
      {
         Random rand = new Random ();
         int numchar = rand.Next (35, 91);
         char letter = Convert.ToChar (numchar);
         return letter;
      }
   }
}