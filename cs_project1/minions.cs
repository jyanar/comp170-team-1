﻿/// This program demonstrates the use of the stopwatch and 
/// provides the code necessary to implement a combat system
/// that requires memorization in order to defeat a set of goblins.
using System;
using System.Diagnostics;
using System.Threading;
using System.Collections.Generic;

namespace IntroCS
{
   class Enemy // Implement interface for dealing with minions/bosses
   {
      // Now for the constructor
      public Enemy()
      {
      }

      //combatLettersequence chunk
      /// This function has the player fight a number of goblins, dependant
      /// on what the argument value was. They get more and more difficult. If 
      /// the player is unable to faithfully replicate the sequence presented 
      /// to him or her, they have health subtracted and are allowed to try again.
      public void combatLetter(int enemies, Health health)
      {
         Console.WriteLine (@"You walk into a group of goblins -- horrid creatures, faces
scarred beyond recognition and carrying rusted, dull swords.");

         var letterSequence = new List<char> ();
         var stopwatch = new Stopwatch ();
         string letterstring = "";
         string userline = "empty!";
         // Now we engage the battle sequence.
         for (int i = 0; i < enemies; i++) {
            Console.Write (@"You get ready to fight the nearest goblin. 
Press Enter to Continue. ");
            Console.ReadLine ();
            letterstring = "";
            while (userline.Trim ().ToUpper () != letterstring) {
               letterstring = "";
               for (int j = 0; j < i + 4; j++) { // The amount of letters presented depend on the goblin number
                  letterSequence.Add (generateUpperletter ()); // Generates uppercase letter, returns as char
                  var chartobedisplayed = letterSequence [letterSequence.Count - 1];
                  Console.Write ("\n    {0}", chartobedisplayed);
                  stopwatch.Start ();
                  // Now that the letter has been displayed for a certain amount of time, clear the line
                  // and display the next letter.
                  while (stopwatch.Elapsed.TotalSeconds < 1.0) {
                     FlushKeyBoardInput ();
                  }
                  // Clear line and reset stopwatch
                  Console.SetCursorPosition (0, Console.CursorTop);
                  ClearCurrentConsoleLine ();
                  stopwatch.Reset ();
                  // Repeat this j times
               }
               // Now we prompt the user for the correct sequence of numbers.
               if (letterSequence.Count == 1) {
                  letterstring = Convert.ToString (letterSequence [0]);
               } else {
                  for (int n = 0; n < letterSequence.Count; n++) {
                     letterstring += Convert.ToString (letterSequence [n]);
                  }
               }

               userline = UI.PromptLine ("Enter the correct sequence: ");
               if (userline.Trim ().ToUpper () == letterstring) {
                  Console.WriteLine ("You stab the goblin, dealing massive damage!");
               } else {
                  Console.WriteLine ("Error! You take damage!");
                  // Implement reduction in health here
               }
               letterSequence.Clear ();
            }
         }
      }

      // Now to implement the six different bosses
      public void bossFirst()
      {
         // Chimera
         string response = "";
         Console.WriteLine ("You have entered into the lair of Chimera. Answer the riddle or DIE!");
         response = UI.PromptLine ("Which creature walks on four legs in the morning, two legs in the " +
         "afternoon, and three legs in the evening?").Trim ().ToLower ();
         while (response != "man") {
            response = UI.PromptLine (" RIDDLE! ");
            if (response != "man") {
               Console.WriteLine ("Wrong! Chimera's snake-like fangs pierce into your skin.");
               // Subtract health
            }
         }
         Console.WriteLine ("Chimera kneels before you, and speaks 'go forth and never return'");
      }
      /*
       * 
            Console.WriteLine ("What invention lets you see through a wall?");
            string response2 = Console.ReadLine (); 
            if (response2 == "window") { 
               Console.WriteLine ("'Go vermin, for I have spared your useless life.");
               Console.WriteLine ();
            } else if (response2 != "window") {
               Console.WriteLine ("WRONG! Raviel grabs you with his talons and chokes you. Answer right before you lose air completely!");
               attempts++;
               Console.WriteLine ();
            } else {
               Console.WriteLine ("You lose air, slowly blacking out, as you hear Raviel's laughter!");
               return;
            }
       * 
       * */
      public void bossSecond()
      {
         // Raviel
         string response = "";
         Console.WriteLine ("LOOK AT ME VERMIN! Answer this and I won't break your bones.");
         while (response != "window") {
            response = UI.PromptLine ("What invention lets you see through a wall?").Trim().ToUpper();
            if (response != "window") {
               Console.WriteLine ("WRONG! Raviel grabs you with his talons and chokes you. Answer right before you lose air completely!");
               // Subtract health
            }
         }

      }





         while (response != "man") {
            response = UI.PromptLine ("Which creature walks on four legs in the morning, two legs in the " +
            "afternoon, and three legs in the evening?").Trim ().ToLower ();
            if (response != "man") {
               Console.WriteLine ("WRONG! Chimera's snake-like fangs pierce into your skin. You cannot " +
               "withstand more of these! Try again!");
               // How are we to take health away here?
            }
         }
            
         }
         string response1 = Console.ReadLine ();
         if (response1 == "man") { 
            Console.WriteLine ("Chimera kneels before you, and speaks 'go forth and never return'");
            Console.WriteLine ();
         } else if (response1 != "man") {
            Console.WriteLine ("WRONG! Chimera's snake-like fangs pierce into your skin. You cannot withstand more of these! Try again!");
            attempts++;
            Console.WriteLine ();
         } else if (attempts == 3) {
            Console.WriteLine ("You took too many bites from Chimera's poisonous fangs, yourn lungs constricted from the venom and you die!");
            return;
         }
      
      public void bossFourth()
      { 

      }

      public void bossFifth()
      {

      }

      public void bossSixth()
      {

      }
            
      // This function clears the current line in the Console
      public static void ClearCurrentConsoleLine ()
      {
         int currentLineCursor = Console.CursorTop;
         Console.SetCursorPosition (0, Console.CursorTop);
         Console.Write (new string (' ', Console.WindowWidth));
         Console.SetCursorPosition (0, currentLineCursor);
      }

      // This function prevents the user from inputting answers while
      // the sequence is displaying.
      public static void FlushKeyBoardInput ()
      {
         while (Console.KeyAvailable) {
            ConsoleKeyInfo key = Console.ReadKey (true);
            Console.Write ("");
         }
      }

      // This function generates a letter within the specified range.
      static char generateUpperletter ()
      {
         Random rand = new Random ();
         int numchar = rand.Next (35, 91);
         char letter = Convert.ToChar (numchar);
         return letter;
      }
   }
}