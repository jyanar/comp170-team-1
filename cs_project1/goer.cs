using System;
using System.Collections.Generic;

namespace IntroCS
{
   /// Response to try to go to a new place.
   public class Goer
   {
      private Game game;
      
      /// Try to go to one direction. If there is an exit, enter the new
      /// place, otherwise print an error message.
      /// Return false(does not end game)
      public bool Execute(Command command)
      {
         // Create dictionary that stores rooms which house enemies. These descriptions
         // are later matched with the description of the current room in order to trigger
         // the appropriate combat sequences. 
         var hostiledict = new Dictionary<string, string> ();
         hostiledict ["arena1"] = "As you slay the last goblin, cold air hits you hard, and you lose your" +
         " footing. A low rumbling permeates the cavern, getting louder and louder. A large beast emerges," +
         " and lands frighteningly close to you. An unearthly combination of flesh, hair, and scale meet" +
         " your eye: with the body of a pegasus, the head of a lion, and a snake-like tongue, the Chimera is" +
         "a rather monstrous beast. The simultaneous roar of a lion and dripping venom of the snake tongue " +
         "paralyze you with fear.";
         hostiledict ["arena2"] = "You hear a loud screech. You look around, and see a shadow flying through " +
         "the air. The winged creature lands in front of you, an aura of violet mist covers the creature.";
         hostiledict ["arena3"] = "You enter a dungeon with snake-like structures all around. You hear the " +
         "slithering of a snake, and you hold your shield firm in your hands. A basilisk slithers out of " +
         "the dark, it's fangs dripping of blood.";
         hostiledict ["arena4"] = "You are transported into the shadow realm, you come face to face with " +
            "Makyura, the destructor.";
         hostiledict["arena5"] = "You are blinded by the light of Horakhty's wings, you try to protect your" +
            " eyes but the light is too strong.";
         hostiledict ["arena6"] = "You have found yourself in a massive Colosseum with a large hole in the" +
            " center. The roars of the thousands of undead fans begin to make the ground shake. A shadowy" +
            " figure begins to rise out of the depths of the hole.";

         if(!command.hasSecondWord()) {
            // if there is no second word, we don't know where to go...
            Console.WriteLine("Go where?");
            return false;
         }
         string direction = command.GetSecondWord ();
         // Try to leave current place.
         Place nextPlace = game.GetCurrentPlace().getExit(direction);
         if (nextPlace == null) {
            Console.WriteLine ("There is a wall in your path.");
         }

         // If there is a valid room and we can enter it, see if it tracks
         // to a specific hostile room. If it does, execute boss/goblin fight. 
         else {
            Console.WriteLine ();
            game.SetCurrentPlace (nextPlace);
            var enemies = new Enemy ();
            string stringplace = nextPlace.GetDescription ().Trim ().ToUpper ().Substring (0, 15);
            var health = new Health (100);
            bool checker = false;

            // Test for each boss room
            if (stringplace == hostiledict ["arena1"].Trim ().ToUpper ().Substring (0, 15)) {
               // Boss #1
               enemies.combatLetter (2);
               Console.WriteLine(nextPlace.GetDescription ());
               enemies.bossFirst (health);
               Console.WriteLine(nextPlace.getExitstring ());
               checker = true;
            } else if (stringplace == hostiledict ["arena2"].Trim ().ToUpper ().Substring (0, 15)) {
               // Boss #2
               enemies.combatLetter (3);
               Console.WriteLine (nextPlace.GetDescription ());
               enemies.bossSecond (health);
               Console.WriteLine (nextPlace.getExitstring ());
               checker = true;
            } else if (stringplace == hostiledict ["arena3"].Trim ().ToUpper ().Substring (0, 15)) {
               // Boss #3
               enemies.combatLetter (4);
               Console.WriteLine (nextPlace.GetDescription ());
               enemies.bossThird (health);
               Console.WriteLine (nextPlace.getExitstring ());
               checker = true;
            } else if (stringplace == hostiledict ["arena4"].Trim ().ToUpper ().Substring (0, 15)) {
               // Boss #4
               enemies.combatLetter (4);
               Console.WriteLine (nextPlace.GetDescription ());
               Console.WriteLine (nextPlace.getExitstring ());
               checker = true;
            } else if (stringplace == hostiledict ["arena5"].Trim ().ToUpper ().Substring (0, 15)) {
               // Boss #5
               enemies.combatLetter (4);
               Console.WriteLine (nextPlace.GetDescription ());
               Console.WriteLine (nextPlace.getExitstring ());
               checker = true;
            } else if (stringplace == hostiledict ["arena6"].Trim ().ToUpper ().Substring (0, 15)) {
               // Boss #6
               enemies.combatLetter (4);
               Console.WriteLine (nextPlace.GetDescription ());
               Console.WriteLine (nextPlace.getExitstring ());
               checker = true;
            }
            if (checker == false) {
               Console.WriteLine (nextPlace.getLongDescription ());
            }
            checker = true;
         }
         return false;
      }
   
      public string Help()
      {
         return @"
Enter
    
   go direction
to exit the current place in the specified direction.
The direction should be in the list of exits for the current place.";
      }

      /// Constructor for objects of class Goer
      public Goer(Game game)
      {
         this.game = game;
      }
   }
}
