/// This cs file is not actually implemented into the project --
/// it is simply the boss code prior to integration.
using System;
using System.Collections.Generic;

namespace Adventure_TeamRocket
{
	class MainClass
	{
		public static void Bosses ()
		{
			int attempts = 0;
			while (attempts < 4) {
				Console.WriteLine ("You have entered into the lair of Chimera. Answer the riddle or DIE!");
				Console.WriteLine ("Which creature walks on four legs in the morning, two legs in the afternoon, and three legs in the evening?");
				string response1 = Console.ReadLine ();
				if (response1 == "man") { 
					Console.WriteLine ("Chimera kneels before you, and speaks 'go forth and never return'");
					Console.WriteLine ();
				} else if (response1 != "man") {
					Console.WriteLine ("WRONG! Chimera's snake-like fangs pierce into your skin. You cannot withstand more of these! Try again!");
					attempts++;
					Console.WriteLine ();
				} else if (attempts == 3) {
					Console.WriteLine ("You took too many bites from Chimera's poisonous fangs, yourn lungs constricted from the venom and you die!");
					return;
				}

				Console.WriteLine ("'LOOK AT ME VERMIN! Answer this and I won't break your bones.");
				Console.WriteLine ("What invention lets you see through a wall?");
				string response2 = Console.ReadLine (); 
				if (response2 == "window") { 
					Console.WriteLine ("'Go vermin, for I have spared your useless life.");
					Console.WriteLine ();
				} else if (response2 != "window") {
					Console.WriteLine ("WRONG! Raviel grabs you with his talons and chokes you. Answer right before you lose air completely!");
					attempts++;
					Console.WriteLine ();
				} else {
					Console.WriteLine ("You lose air, slowly blacking out, as you hear Raviel's laughter!");
					return;
				}

				Console.WriteLine ("Answer this riddle before he strikes you!");
				Console.WriteLine ("Who makes it, has no need of it.\nWho buys it, has no use for it. \nWho uses it can neither see nor feel it. \nWhat is it?");
				string response3 = Console.ReadLine ();
				if (response3 == "coffin") { 
					Console.WriteLine ("You slit the basilisk's throat with your sword, go forth before any other danger befalls you.");
					Console.WriteLine ();
				} else if (response3 != "coffin") {
					Console.WriteLine ("Wrong! The basilisk lunges forward with its fangs awide.");
					attempts++;
					Console.WriteLine ();
				} else if (attempts == 4) {
					Console.WriteLine ("The basilisk wraps around you and sinks its fangs into you.");
					return;
				}

				Console.WriteLine ("'Answer this riddle if you want to return mortal'");
				Console.WriteLine ("Feed me and I live, yet give me a drink and I die.");
				string response4 = Console.ReadLine (); 
				if (response4 == "fire") { 
					Console.WriteLine ("You are transported back. You are out of breath, but you are able to go forth in your challenge.");
					Console.WriteLine ();
				} else if (response4 != "fire") {
					Console.WriteLine ("WRONG! 'You don't have much time mortal, answer me if you want to live!");
					attempts++;
					Console.WriteLine ();
				} else if (attempts == 4) {
					Console.WriteLine ("You have been imprisoned in the shadow realm");
					return;
				}

				Console.WriteLine ("'Answer this before you die human!");
				Console.WriteLine ("What is greater than God,\nmore evil than the devil,\nthe poor have it,\nthe rich need it,\nand if you eat it, you'll die?");
				string response5 = Console.ReadLine (); 
				if (response5 == "nothing") { 
					Console.WriteLine ("You have defeated _______ . ");
					Console.WriteLine ();
				} else if (response5 != "nothing") {
					Console.WriteLine ("WRONG! You missed your strike, and Horakhty laughs at you. Try again before you die.");
					attempts++;
					Console.WriteLine ();
				} else if (attempts == 4) {
					Console.WriteLine ("You failed to kill Horakhty, and you bleed out after his attacks.");
					return;
				}

				Console.WriteLine ("The Chaos Dragon enters, answer this riddle so you may find the amulet.");
				Console.WriteLine ("What occurs once in every minute, twice in every moment, yet never in a thousand years?");
				string response6 = Console.ReadLine (); //man
				if (response6 == "m") { 
					Console.WriteLine ("You have defeated the Chaos Dragon and wringed the amulet from his throat.");
					Console.WriteLine ();
				} else if (response6 != "m") {
					Console.WriteLine ("WRONG! You are so close, yet so far. Fight until you die, answer again!");
					attempts++;
					Console.WriteLine ();
				} else if (attempts == 4) {
					Console.WriteLine ("You failed, so came so far, but you could not defeat the dragon.");
					return;
				}
			}

		}

	}
}
