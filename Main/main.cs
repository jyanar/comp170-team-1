﻿using System;

namespace IntroCS
{
   class TestIntroduction
   {
      public static void Main(string[] args)
      {
         // This tests the introduction class
         var introduction = new Introduction();
         introduction.IntroMenu();
      }
   }
}