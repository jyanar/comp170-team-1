﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Collections.Generic;

namespace IntroCS
{
	public class Enemies
	{
		private int numofenemies;

		public Enemies(int goblin)
		{
			numofenemies = goblin;
		}

		//combatLettersequence chunk
		/// This function has the player fight a number of goblins, dependant
		/// on what the input value was. They get more and more difficult. If 
		/// the player is unable to faithfully replicate the sequence presented 
		/// to him or her, they have health subtracted and are allowed to try again.
		public void combatLettersequence()
		{
			Console.WriteLine (@"You walk into a group of goblins -- horrid creatures, faces
scarred beyond recognition and carrying rusted, dull swords.");
			var letterSequence = new List<char> ();
			var stopwatch = new Stopwatch ();
			string letterstring = "";
			string userline = "empty!";
			// Now we engage the battle sequence.
			for (int i = 0; i < 5; i++) {
				Console.WriteLine ();
				Console.Write (@"You get ready to fight the nearest goblin. 
Press Enter to Continue. ");
				Console.WriteLine ();
				letterstring = "";
				Console.ReadLine ();
				while (userline.Trim().ToUpper() != letterstring) {
					letterstring = "";
					for (int j = 0; j < i + 1; j++) {
						// Implement function that randomly chooses character in some
						// desired range, and have that print on the screen as well
						// as be recorded to the list.

						letterSequence.Add (generateUpperletter ()); // Generates uppercase letter, returns as char
						var chartobedisplayed = letterSequence [letterSequence.Count - 1];
						Console.Write ("\n  {0}", chartobedisplayed);
						stopwatch.Start ();
						// Now that the letter has been displayed for a certain amount of time, clear the line
						// and display the next letter.
						while (stopwatch.Elapsed.TotalSeconds < 1.0) {
							Console.Write ("");
						}
						// Clear line and reset stopwatch
						Console.SetCursorPosition (0, Console.CursorTop);
						ClearCurrentConsoleLine ();
						stopwatch.Reset ();

					}
					// Now we prompt the user for the correct sequence of numbers.
					if (letterSequence.Count == 1) {
						letterstring = Convert.ToString (letterSequence [0]);
					} else {
						for (int n = 0; n < letterSequence.Count; n++) {
							letterstring += Convert.ToString (letterSequence [n]);
						}
					}

					userline = UI.PromptLine ("Enter the correct sequence: ");
					if (userline.Trim ().ToUpper () == letterstring) {
						Console.WriteLine ("You stab the goblin, dealing massive damage!");
					} else {
						Console.WriteLine ("Error! You take damage!");

					}

					letterSequence.Clear ();
				}
			}
		}
		// This function clears the current line in the Console
		public static void ClearCurrentConsoleLine ()
		{
			int currentLineCursor = Console.CursorTop;
			Console.SetCursorPosition (0, Console.CursorTop);
			Console.Write (new string (' ', Console.WindowWidth));
			Console.SetCursorPosition (0, currentLineCursor);
		}
		// This function generates a letter within the specified range.
		static char generateUpperletter ()
		{
			Random rand = new Random ();
			int numchar = rand.Next (65, 91);
			char letter = Convert.ToChar (numchar);
			return letter;
		}
	}
}  

