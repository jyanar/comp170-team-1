﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Collections.Generic;

namespace IntroCS
{
	class Introduction 
	{
      // Constructor
      public Introduction()
      {
      }
         
		public void IntroMenu()
		{
         string response = "";
      	Header ();
			Console.WriteLine ("======= Please select an option to continue! =======");
			Console.WriteLine ("==== N=New Game ==== I=Instructions ==== E=Exit ====");
         Console.WriteLine ("====================================================");
			response = Console.ReadLine ();
			response = response.ToUpper ();

			if (response == "N") {
				Console.Clear ();
				Console.WriteLine ("What would you like to name your character?");
				string playerName = Console.ReadLine ();
				Console.WriteLine ();
				Console.WriteLine ("Well," + playerName + " ");
				Enemies goblin = new Enemies (5);
				goblin.combatLettersequence ();
			} if (response == "I") {
				Console.Clear ();
				Instructions ();
				Console.WriteLine ();


			} else if (response == "E") {
				Console.Clear ();
				Console.WriteLine ("");
				return;
			}
		}
		
      public void Instructions ()
		{
			string response = "";
			Console.WriteLine(@"Within DungeonCrawler, you'll be tasked with retrieving the Amulet of Yendor.
In order to play the game, use commands:
");
			Console.WriteLine ("======= Please select an option to continue! =======");
			Console.WriteLine ("==== N=New Game ==== E=Exit ====");
         Console.WriteLine ("================================");
			response = Console.ReadLine ();
			response = response.ToUpper ();
			if (response == "N") {
				Console.Clear ();
				Console.WriteLine ("What would you like to name your character?");
				string playerName = Console.ReadLine ();
				Console.WriteLine ();
				Console.WriteLine ("Well," + playerName + " ");
				Enemies goblin = new Enemies (5);
				goblin.combatLettersequence ();
			}
			else if (response == "E") {
				Console.Clear ();
				Console.WriteLine ("");
				return;
			}


		}

      public void Header()
		{
			Console.ForegroundColor = ConsoleColor.Green;
			
			Console.WriteLine (@"=========================================================
== Welcome to Dungeon Crawler - A text based adventure ==
============ game created by Team Rocket ================");
         Console.WriteLine ();
			
			Console.ForegroundColor = ConsoleColor.Cyan;
			Console.WriteLine (@"The Map to the Dungeon will guide you to the Potion that 
has been hidden for centuries by the Almighty Robert Yacobellis.");
			Console.ForegroundColor = ConsoleColor.Red;
			Console.WriteLine (@"
            _____________________________
           |                             |
           |___     _______________      |
               |   |          ___  |     |______
               |___|         |   | |            |
                             |   | |_______     |________
        ________        _____|   |         |             |
 Enter:         |      |         |         |________     |
        _____   |______|         |         |             |
             |               |   |  _______|       ______|
             |____      _____|   | |              |
                  |    |     |   |_|________      |
                  |____|     |                    |        
                             |____________________|
");
		}
	}
} 
