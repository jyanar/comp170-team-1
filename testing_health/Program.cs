﻿using System;

namespace testing_health
{
   public class Health
   {
      private string name;
      private int startinghealth = 100;
      private int health; 

      // Constructor
      public Health(string name, int health)
      {
         this.name = name;
         this.health = health;
      }

      //subtractHealth chunk
      // This method subtracts a certain amount of health
      // from the player and displays however much is left. 
      public static void subtractHealth(int num)
      {
         health = health - num;
         Console.WriteLine (@"You take {0} damage!
Current health: ", num);
      }
   }
}
