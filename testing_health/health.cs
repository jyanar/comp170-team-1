﻿using System;

namespace IntroCS
{
   class Health
   {
      private string name;
      private int health; 

      // Constructor
      public Health(string name, int health)
      {
         this.name = name;
         this.health = health;
      }

      //subtractHealth chunk
      // This method subtracts a certain amount of health
      // from the player and displays however much is left. 
      public void subtractHealth(int num)
      {
         health = health - num;
         Console.WriteLine (@"You take {0} damage!
Current health: {0} / 100", health); // Out of convention, health is defaulted to 100. 
      }

      public void addHealth(int num)
      {
         health = health + num;
         Console.WriteLine (@"Your health is replenished by {0}!
Current health: {0} / 100", health);
      }
   }
}
